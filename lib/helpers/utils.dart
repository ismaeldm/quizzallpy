import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

final Color roxo = Color.fromARGB(255, 72, 70, 151);
final Color roxinho = Color.fromARGB(255, 142, 124, 195);
final Color roxoAzul = Color.fromARGB(255, 131, 128, 212);
final Color rosa = Color.fromARGB(255, 222, 45, 91);
final Color verde = Color.fromARGB(255, 0, 146, 123);
final Color vermelho = Color.fromARGB(255, 251, 0, 88);
final Color textoCinza = Color.fromARGB(255, 80, 80, 80);

extension StringUtils on String {
  static const diacritics =
      'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
  static const nonDiacritics =
      'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

  String get removeAccents => this
      .split('')
      .map((String char) {
    int index = diacritics.indexOf(char);
    return index >= 0 ? nonDiacritics[index] : char;
  })
      .join();

  String get onlyNumbers => getOnlyNumbers(this, '');
  String get capitalizeFirst => this.isEmpty ? '' : '${this[0].toUpperCase()}${this.substring(1, this.length)}';
  String get capitalize => this.split(' ').map((String word) => word.capitalizeFirst).join(' ');
}

bool _showingLoading = false;
Future? showLoading(BuildContext context, {String? message}) {
  if (!_showingLoading) {
    _showingLoading = true;
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: message == null ? CircularProgressIndicator() : Container(
              width: MediaQuery.of(context).size.width * 0.4,
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.width * 0.05),
              child: Material(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.05,
                      height: MediaQuery.of(context).size.width * 0.05,
                      child: CircularProgressIndicator(),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                      child: Text(message, style: TextStyle(fontSize: 14, color: Colors.black, fontWeight: FontWeight.w300)),
                    )
                  ],
                )
              ),
            ),
          ),
        );
      }
    );
  }
}

closeLoading(BuildContext context) {
  if (_showingLoading) {
    Navigator.pop(context);
    _showingLoading = false;
  }
}

String getOnlyNumbers(String s, [String otherCharsAccepted = '']) {
  String number = '';
  s.characters.forEach((char) {
    if ('0123456789$otherCharsAccepted'.contains(char)) {
      number += char;
    }
  });
  return number;
}

void showToast(String message, Color backgroundColor) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    backgroundColor: backgroundColor,
    textColor: Colors.white,
    fontSize: 16.0
  );
}