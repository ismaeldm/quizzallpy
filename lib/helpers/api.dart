import 'dart:collection';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:quizzallpy/models/AnswerModel.dart';
import 'package:quizzallpy/models/QuizModel.dart';

class Api {

  final String serverAddress = 'https://quizzallpy-default-rtdb.firebaseio.com/';
  final Dio dio = Dio();
  late String auth;

  Api() {
    dio.options = BaseOptions(
      contentType: 'application/json',
      responseType: ResponseType.plain,
      connectTimeout: 10000,
      receiveTimeout: 100000,
    );
  }

  void _validateError(error) {
    //Se for necessário fazer uma validação global, como por exemplo ao renovar o token do login, é possível tratar por aqui
    print(error);
  }

  Future<String> login() async {
    try {
      Response response = await dio.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBqhAw5qPZ_75AQ3U9bRkfgK0Ldd5kmppk',
        data: '{"email":"ismael.dullius.machado@hotmail.com","password":"ismael","returnSecureToken":true}',
      );
      Map<String, dynamic> json = jsonDecode(response.data);
      if (json['idToken'] is String) {
        auth = '?auth=${json['idToken']}';
        return '';
      }
      throw 'Retorno não é do tipo esperado:\n${response.data}';
    } catch(e) {
      _validateError(e);
      return e.toString();
    }
  }

  Future<List<QuizModel>> loadQuiz() async {
    List<QuizModel> items = [];
    try {
      Response response = await dio.get('https://quizzallpy-default-rtdb.firebaseio.com/quiz.json$auth');
      jsonDecode(response.data).forEach((json) => items.add(QuizModel.fromJson(json)));
    } catch(e) {
      _validateError(e);
    }
    return items;
  }

  Future<bool> addAnswer(String userName, double accuracy) async {
    try {
      await dio.post(
        'https://quizzallpy-default-rtdb.firebaseio.com/answers.json$auth',
        data: {'user': userName, 'percent': accuracy*100}
      );
      return true;
    } catch(e) {
      _validateError(e);
    }
    return false;
  }

  Future<List<AnswerModel>> loadAnswers() async {
    List<AnswerModel> items = [];
    try {
      Response response = await dio.get('https://quizzallpy-default-rtdb.firebaseio.com/answers.json$auth');
      Map<String, dynamic> json = jsonDecode(response.data);
      json.forEach((_, _json) => items.add(AnswerModel.fromJson(_json)));
      items.sort((a, b) => (b.percent - a.percent).truncate());
    } catch(e) {
      _validateError(e);
    }
    return items;
  }

}