import 'package:quizzallpy/helpers/utils.dart';
import 'package:quizzallpy/view/CircularProgress.dart';
import 'package:quizzallpy/view/TextWithIcon.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

BehaviorSubject createBehaviorSubject() {
  return BehaviorSubject<StreamState>.seeded(StreamStateInitial());
}

class LoadPage extends StatelessWidget {

  final BehaviorSubject<StreamState> stream;
  final GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  final  RefreshCallback onRefresh;
  final EdgeInsets padding;
  final Future<StreamStateSuccess> Function()? onLoadNext;

  LoadPage({
    Key? key,
    required this.stream,
    required this.refreshIndicatorKey,
    required this.onRefresh,
    this.padding = EdgeInsets.zero,
    this.onLoadNext,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: stream,
      builder: (context, AsyncSnapshot<StreamState> snapshot) {
        if (snapshot.data is StreamStateLoading) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Center(child: CircularProgress())
          );
        }

        if (snapshot.data is StreamStateSuccess || snapshot.data is StreamStateError) {
          Widget widgetContent;
          if (snapshot.data is StreamStateSuccess) {
            List<Widget> items = (snapshot.data as StreamStateSuccess).content;
            if (
              (snapshot.data as StreamStateSuccess).hasNext() &&
              onLoadNext != null &&
              items.where((item) => item.runtimeType == LoadMoreButton).isEmpty
            ) {
              items.add(LoadMoreButton(onLoadNext!));
            }
            widgetContent = SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: padding,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: items,
              ),
            );
          } else {
            widgetContent = SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(vertical: 70, horizontal: 20),
              child: Text(
                (snapshot.data as StreamStateError).message,
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.justify
              )
            );
          }

          return RefreshIndicator(
            key: refreshIndicatorKey,
            child: widgetContent,
            onRefresh: onRefresh
          );
        }

        return Container();
      }
    );
  }
}

class LoadMoreButton extends StatefulWidget {

  final Future<StreamStateSuccess> Function() onPressed;
  final List<Widget> _items = [];
  LoadMoreButton(this.onPressed);

  @override
  _LoadMoreButtonState createState() => _LoadMoreButtonState();
}

class _LoadMoreButtonState extends State<LoadMoreButton> {

  bool loading = false;
  bool hasNext = true;

  void loadMore() async {
    setState(() => loading = true);
    try {
      StreamStateSuccess streamState = await widget.onPressed();
      widget._items.removeLast(); //Remover circular progress
      widget._items.addAll(streamState.content);
      hasNext = streamState.hasNext();
    } finally {
      setState(() => loading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (hasNext) {
      hasNext = false;
      widget._items.add(TextButton(
        child: TextWithIcon(
          text: Text('Carregar mais...', style: TextStyle(color: roxo)),
          icon: Icons.sync,
          iconSize: 20,
          direction: TextDirection.rtl,
        ),
        onPressed: loadMore
      ));
    }
    if (loading) {
      widget._items.removeLast(); //Remove load more button
      widget._items.add(
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: Center(
            child: CircularProgress(small: true, color: roxo),
          )
        )
      );
    }
    return widget._items.isEmpty ? Container() : Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: widget._items,
    );
  }
}

abstract class StreamState {}

class StreamStateInitial extends StreamState{
  @override
  String toString() {
    return "StreamStateInitial";
  }
}

class StreamStateLoading extends StreamState{
  @override
  String toString() {
    return "StreamStateLoading";
  }
}

class StreamStateDisconnect extends StreamState{
  @override
  String toString() {
    return "StreamStateDisconnect";
  }
}

class StreamStateError extends StreamState{
  final String message;

  StreamStateError({required this.message});

  @override
  String toString() {
    return "StreamStateError: " + message;
  }
}

class StreamStateSuccess extends StreamState{
  final dynamic content;
  final bool Function() hasNext;

  StreamStateSuccess({required this.content, required this.hasNext});

  @override
  String toString() {
    return "StreamStateSuccess: " + content.toString();
  }
}