class AnswerModel {
  String user = '';
  double percent = 0;

  AnswerModel.fromJson(Map<String, dynamic> json) {
    user = json['user'] ?? user;
    percent = json['percent']+0.0 ?? percent;
  }
}