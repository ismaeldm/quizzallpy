class QuizModel {
  String brand = '';
  String correct = '';
  String logo = '';
  List<String> options = [];

  String optionSelected = ''; //calculated field

  QuizModel.fromJson(Map<String, dynamic> json) {
    brand = json['brand'] ?? brand;
    correct = json['correct'] ?? correct;
    logo = json['logo'] ?? logo;
    options = json['options'] is List ? List.castFrom(json['options']) : options;
  }
}