import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:quizzallpy/view/ImageSplashScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ImageSplashScreen(),
      title: 'QuizZallpy',
      routes: <String, WidgetBuilder> {
        "/ImageSplashScreen" : (context) => ImageSplashScreen(),
      },
      theme: ThemeData(
        canvasColor: Colors.black,
        fontFamily: 'Roboto',
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [const Locale('pt', 'BR')],
      locale: const Locale('pt', 'BR'),
    )
  );
}