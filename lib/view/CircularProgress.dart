import 'package:flutter/material.dart';
import 'package:quizzallpy/helpers/utils.dart';

class CircularProgress extends StatelessWidget {

  final bool small;
  final Color? color;

  const CircularProgress({Key? key, this.small = false, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget circular = CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(color ?? (small ? Colors.white : roxo)),
      strokeWidth: small ? 2 : 3,
    );

    return small ? SizedBox(height: 20, width: 20, child: circular) : circular;
  }
}
