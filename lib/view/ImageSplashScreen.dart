import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:quizzallpy/helpers/api.dart';
import 'package:quizzallpy/helpers/utils.dart';
import 'package:quizzallpy/view/home/home.dart';

class ImageSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<ImageSplashScreen> {

  String title = '';
  bool jaIniciou = false;
  Api api = Api();

  void _afterBuild(BuildContext context) async {
    try {
      String loginError = await api.login();
      if (loginError.isEmpty) {
        await Future.delayed(Duration(milliseconds: 400));
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            settings: const RouteSettings(name: '/home'),
            builder: (context) => Home(api: api)
          )
        );
      } else {
        setState(() => title = "Ops! Não foi possível fazer o login no aplicativo.\n$loginError");
      }
    } catch(e) {
      setState(() => title = "Ops! Ocorreu um erro ao entrar no aplicativo. Favor tente novamente mais tarde!");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!jaIniciou) {
      jaIniciou = true;
      WidgetsBinding.instance!.addPostFrameCallback((_) => _afterBuild(context));
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: roxo,
        body: Center(
          child: Image.asset('assets/logo.png', width: MediaQuery.of(context).size.width * 0.6)
        ),
        bottomNavigationBar: title.isEmpty ? SizedBox(height: 0) : Text(
          '$title\n',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      )
    );
  }
}