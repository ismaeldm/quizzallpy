import 'package:flutter/material.dart';
import 'package:quizzallpy/view/CircularProgress.dart';
import 'package:quizzallpy/view/TextWithIcon.dart';

class LoadButton extends StatefulWidget {

  final String title;
  final Future Function() onPressed;
  final Color backgroundColor;
  final IconData? icon;
  final EdgeInsets padding;
  const LoadButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.backgroundColor = const Color.fromARGB(255, 0, 146, 123),
    this.icon,
    this.padding = const EdgeInsets.symmetric(horizontal: 20)
  }) : super(key: key);

  @override
  _LoadButtonState createState() => _LoadButtonState();
}

class _LoadButtonState extends State<LoadButton> {

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    Widget content;
    if (loading) {
      content = Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: CircularProgress(small: true)
      );
    } else {
      content = Text(widget.title, style: TextStyle(color: Colors.white));
      if (widget.icon != null) {
        content = TextWithIcon(
          text: content as Text,
          icon: widget.icon!,
          iconSize: 16,
        );
      }
    }
    return TextButton(
      child: content,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(widget.backgroundColor),
        padding: MaterialStateProperty.all(widget.padding),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
        elevation: MaterialStateProperty.all(5)
      ),
      onPressed: () async {
        if (!loading) {
          setState(() => loading = true);
          try {
            await widget.onPressed();
          } finally {
            setState(() => loading = false);
          }
        }
      },
    );
  }
}
