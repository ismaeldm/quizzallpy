import 'package:flutter/material.dart';
import 'package:quizzallpy/helpers/utils.dart';

class AppPage extends StatelessWidget {

  final Widget content;
  final bool isMainPage;
  final bool showWillPop;
  final EdgeInsets padding;
  const AppPage({
    Key? key,
    required this.isMainPage,
    required this.content,
    this.showWillPop = true,
    this.padding = EdgeInsets.zero
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return showWillPop ? ((await showDialog(
          context: context,
          builder: (context) =>
            AlertDialog(
              title: Text('Tem certeza?', style: TextStyle(color: textoCinza)),
              content: Text(
                isMainPage ? 'Deseja sair do Quiz Zallpy?' : 'Deseja voltar a tela principal?',
                style: TextStyle(color: textoCinza),
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text('Não', style: TextStyle(color: verde)),
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text('Sim', style: TextStyle(color: vermelho)),
                ),
              ],
            ),
        )) ?? false) : true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Quiz Zallpy',
            style: TextStyle(fontFamily: 'Comicsans')
          ),
          toolbarHeight: 40,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        extendBodyBehindAppBar: true,
        body: Container(
          decoration: BoxDecoration(
            color: roxo,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/background.jpg')
            )
          ),
          padding: EdgeInsets.only(top: 70, left: 10, right: 10),
          height: MediaQuery.of(context).size.height,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
              color: Colors.white.withAlpha(220),
              boxShadow: [
                BoxShadow(blurRadius: 2, color: roxo)
              ]
            ),
            padding: padding,
            child: content,
          ),
        ),
      )
    );
  }
}
