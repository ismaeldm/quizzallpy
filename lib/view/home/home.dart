import 'package:flutter/material.dart';
import 'package:quizzallpy/helpers/api.dart';
import 'package:quizzallpy/helpers/utils.dart';
import 'package:quizzallpy/models/AnswerModel.dart';
import 'package:quizzallpy/models/QuizModel.dart';
import 'package:quizzallpy/view/AppPage.dart';
import 'package:quizzallpy/view/LoadButton.dart';
import 'package:quizzallpy/view/home/quiz.dart';
import 'package:quizzallpy/view/home/ranking.dart';

class Home extends StatelessWidget {

  final Api api;
  const Home({Key? key, required this.api}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppPage(
      isMainPage: true,
      padding: EdgeInsets.all(25),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '\nSeja bem vindo ao Quiz Zallpy!',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: rosa),
          ),
          Expanded(
            child: Center(child: Text(
              'Seu objetivo aqui é tentar acertar o país de origem de 5 montadoras diferentes, ao final será exibido um ranking com os melhores jogadores e sua posição nele.\n\nSerá que você consegue alcançar o 1º lugar? :)\n',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: textoCinza,
                height: 1.4
              ),
            )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LoadButton(
                title: 'Começar',
                onPressed: () async {
                  List<QuizModel> quiz = await api.loadQuiz();
                  if (quiz.isNotEmpty) {
                    quiz.shuffle();
                    String userName = await getUserName(context);
                    if (userName.isNotEmpty) {
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Quiz(api: api, quiz: quiz, userName: userName)),
                      ).then((value) async {
                        if (value is bool && value) {
                          showLoading(context);
                          List<AnswerModel> ranking;
                          try {
                            ranking = await api.loadAnswers();
                          } finally {
                            closeLoading(context);
                          }
                          openRanking(context, ranking, userName);
                        }
                      });
                    }
                  } else {
                    showToast('Ocorreu algum problema, favor tente novamente mais tarde', vermelho);
                  }
                }
              ),
              SizedBox(width: 20),
              LoadButton(
                title: 'Ver Ranking',
                icon: Icons.emoji_events_outlined,
                backgroundColor: Colors.amber[700]!,
                onPressed: () async {
                  openRanking(context, await api.loadAnswers(), null);
                }
              ),
            ],
          )

        ],
      )
    );
  }

  Future<String> getUserName(BuildContext context) async {
    TextEditingController controller = TextEditingController();
    bool ok = await showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Finalizar Quiz', style: TextStyle(color: roxo, fontSize: 16)),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: TextField(
            textCapitalization: TextCapitalization.sentences,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 15),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: roxinho)
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: roxinho),
                borderRadius: BorderRadius.circular(10)
              ),
              labelText: 'Informe seu nome',
              labelStyle: TextStyle(color: roxoAzul),
              hintText: 'Que aparecerá no ranking',
              hintStyle: TextStyle(fontSize: 14, color: textoCinza),
            ),
            style: TextStyle(color: textoCinza),
            controller: controller,
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Cancelar', style: TextStyle(color: textoCinza)),
            onPressed: () => Navigator.of(context).pop(false),
          ),
          TextButton(
            child: Text('Confirmar', style: TextStyle(color: verde)),
            onPressed: () => Navigator.of(context).pop(true),
          ),
        ],
      );
    }) ?? false;

    if (ok) {
      return controller.text;
    }
    return '';
  }

  void openRanking(BuildContext context, List<AnswerModel> ranking, String? userName) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => Ranking(ranking: ranking, userName: userName)),
    );
  }
}
