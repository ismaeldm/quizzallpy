import 'package:flutter/material.dart';
import 'package:quizzallpy/helpers/api.dart';
import 'package:quizzallpy/helpers/utils.dart';
import 'package:quizzallpy/models/QuizModel.dart';
import 'package:quizzallpy/view/AppPage.dart';
import 'package:quizzallpy/view/CircularProgress.dart';
import 'package:quizzallpy/view/LoadButton.dart';

class Quiz extends StatefulWidget {

  final Api api;
  final List<QuizModel> quiz;
  final String userName;
  const Quiz({Key? key, required this.api, required this.quiz, required this.userName}) : super(key: key);

  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {

  String selectedOption = '';
  bool showCorrect = false;
  int currentQuizIndex = 0;

  @override
  Widget build(BuildContext context) {
    return AppPage(
      isMainPage: false,
      padding: EdgeInsets.all(25),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              '${currentQuizIndex+1} / ${widget.quiz.length}',
              textAlign: TextAlign.end,
              style: TextStyle(color: textoCinza),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30),
              child: FadeInImage(
                image: NetworkImage(widget.quiz[currentQuizIndex].logo),
                placeholder: AssetImage(''),
                placeholderErrorBuilder: (_, __, ___) => Center(child: CircularProgress()),
                imageErrorBuilder: (_, __, ___) => SizedBox(
                  height: 150,
                  child: Center(
                    child: Text(
                      widget.quiz[currentQuizIndex].brand,
                      style: TextStyle(fontSize: 20),
                    )
                  )
                ),
                fit: BoxFit.contain,
                width: MediaQuery.of(context).size.width - 100,
                height: 150,
              ),
            ),
            ...widget.quiz[currentQuizIndex].options.map(
              (String option) {
                bool isCorrect = showCorrect && option.removeAccents.toLowerCase() == widget.quiz[currentQuizIndex].correct;
                return RadioListTile(
                  value: option,
                  groupValue: selectedOption,
                  activeColor: isCorrect ? verde : (showCorrect ? vermelho : roxo),
                  title: Text(
                    option,
                    style: TextStyle(color: isCorrect ? verde : (showCorrect ? vermelho : Colors.black)),
                  ),
                  contentPadding: EdgeInsets.zero,
                  onChanged: (_) {
                    setState(() => selectedOption = option);
                  }
                );
            }).toList(),
            SizedBox(height: 20),
            Center(
              child: LoadButton(
                title: currentQuizIndex == widget.quiz.length-1 ? 'Finalizar' : 'Próximo',
                padding: EdgeInsets.symmetric(horizontal: 50),
                onPressed: () async {
                  if (selectedOption.isNotEmpty) {
                    widget.quiz[currentQuizIndex].optionSelected = selectedOption.removeAccents.toLowerCase();
                    setState(() => showCorrect = true);
                    if (currentQuizIndex < widget.quiz.length-1) {
                      await Future.delayed(Duration(seconds: 1));
                      setState(() {
                        selectedOption = '';
                        showCorrect = false;
                        currentQuizIndex++;
                      });
                    } else {
                      int correctAnswers = 0;
                      widget.quiz.forEach((quiz) => quiz.correct == quiz.optionSelected ? correctAnswers++ : null);
                      if (await widget.api.addAnswer(widget.userName, correctAnswers/widget.quiz.length)) {
                        Navigator.of(context).pop(true);
                      } else {
                        showToast("Ocorreu algum problema, favor tente novamente mais tarde", vermelho);
                        setState(() => showCorrect = false);
                      }
                    }
                  }
                }
              )
            )
          ],
        )
      )
    );
  }
}
