import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:quizzallpy/helpers/utils.dart';
import 'package:quizzallpy/models/AnswerModel.dart';
import 'package:quizzallpy/view/AppPage.dart';

class Ranking extends StatelessWidget {

  final List<AnswerModel> ranking;
  final String? userName;
  const Ranking({Key? key, required this.ranking, this.userName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> listRanking = [];
    int userIndex = -1;
    for (int i=0; i<ranking.length; i++) {
      userIndex = ranking[i].user == userName ? i : userIndex;
      Color textColor = textoCinza;
      switch (i) {
        case 0:
          textColor = Colors.amber[600]!;
          break;

        case 1:
          textColor = Color.fromARGB(255, 192, 192, 192);
          break;

        case 2:
          textColor = Color.fromARGB(255, 205, 127, 50);
          break;
      }
      listRanking.add(
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20, right: 10),
              child: i >= 3 ? Text(
                ' ${i+1}°',
                style: TextStyle(color: textoCinza)
              ) : Image.asset('assets/medal${i+1}.png', width: 16, height: 16),
            ),
            Text(
              '${ranking[i].user} (${ranking[i].percent.truncate()}%)',
              style: TextStyle(
                color: textColor,
                fontWeight: i >= 3 ? FontWeight.normal : FontWeight.bold,
                height: 1.6
              )
            ),
          ],
        )
      );
    }
    if (userIndex >= 0) {
      int userPercent = ranking[userIndex].percent.truncate();
      listRanking.insert(0, SizedBox(
        height: 270,
        width: 270,
        child: Stack(
          children: [
            PieChart(
              PieChartData(
                borderData: FlBorderData(show: false),
                sectionsSpace: 0,
                centerSpaceRadius: 60,
                sections: [
                  PieChartSectionData(
                    color: Colors.grey[50],
                    value: 100-userPercent-0.0,
                    showTitle: false,
                    radius: 45,
                  ),
                  PieChartSectionData(
                    color: roxoAzul,
                    value: userPercent+0.0,
                    radius: 50,
                    showTitle: false,
                  ),
                ]
              ),
            ),
            Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: TextStyle(color: roxo),
                  children: <TextSpan>[
                    TextSpan(text: '$userPercent%', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                    TextSpan(text: '\nde acerto', style: TextStyle(fontSize: 14)),
                  ]
                ),
              ),
            )
          ]
        )),
      );
      listRanking.insert(0, Text(
        '${userIndex+1}º é a sua posição!',
        textAlign: TextAlign.center,
        style: TextStyle(color: verde, fontSize: 18),
      ));
    }
    return AppPage(
      isMainPage: false,
      showWillPop: false,
      content: SingleChildScrollView(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: listRanking,
        ),
      )
    );
  }
}
