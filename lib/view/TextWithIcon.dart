import 'package:flutter/material.dart';

class TextWithIcon extends StatelessWidget {

  final Text text;
  final IconData icon;
  final TextDirection direction;
  final MainAxisAlignment alignment;
  final double spacing;
  final double? iconSize;
  final Color? iconColor;
  TextWithIcon({
    required this.text,
    required this.icon,
    this.direction = TextDirection.ltr,
    this.alignment = MainAxisAlignment.end,
    this.spacing = 10,
    this.iconSize,
    this.iconColor,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: direction,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: alignment,
      children: [
        text,
        SizedBox(width: spacing),
        Icon(
          icon,
          color: iconColor ?? (text.style?.color ?? Colors.black),
          size: iconSize ?? (text.style?.fontSize ?? 14)
        )
      ],
    );
  }
}
